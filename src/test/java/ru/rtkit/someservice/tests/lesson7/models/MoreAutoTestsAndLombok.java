package ru.rtkit.someservice.tests.lesson7.models;

import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.TmsLink;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ru.rtkit.someservice.apiHelper.Endpoints;
import ru.rtkit.someservice.tests.BaseTest;

public class MoreAutoTestsAndLombok extends BaseTest {
    @Test
    void workWithProperties(){
        System.out.println(props);
        System.out.println(props.getProperty("serviceName"));
        Assumptions.assumeTrue(props.getProperty("serviceName").equals("petstore"));
        System.out.println(props.getProperty("seviceName"));

    }

    @Test
    @Severity(SeverityLevel.CRITICAL)
    @DisplayName("create user")
    @TmsLink("189")
    void checkUserCreation(){
        User userReq = new User();
        userReq.setId(25L);
        userReq.setUsername("Гена");
        userReq.setFirstName("Генадий");
        userReq.setLastName("Трубогиб");
        userReq.setEmail("gena2025@ma.ru");

        apiHelper.post(Endpoints.USER, userReq, resp200);
        User userActual = apiHelper.get(Endpoints.USERNAME, resp200, userReq.getUsername()).as(User.class);
        Assertions.assertEquals(userReq.getEmail(), userActual.getEmail());
    }
}

package ru.rtkit.someservice.tests.lesson7.homework;

import lombok.Getter;
import lombok.Setter;

public class StoreOrderApiRequestLombok {
    @Getter
    @Setter
    private int id;

    @Getter
    @Setter
    private int petId;

    @Getter
    @Setter
    private int quantity;

    @Getter
    @Setter
    private String shipDate;

    @Getter
    @Setter
    private String status;

    @Getter
    @Setter
    private boolean complete;
}
